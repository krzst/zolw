package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public static final int KROK_W_PIKSELACH = 10;

    public static final double RADIAN = 0.017453292519d;

    @FXML
    Pane plac;

    @FXML
    CheckBox rysuj;

    @FXML
    TextField krokiPole;

    @FXML
    TextField stopniePole;

    @FXML
    Label operacjaLabelka;

    @FXML
    Circle zolw;

    @FXML
    Slider krokiSuwak;

    @FXML
    Slider stopnieSuwak;

    private List<Line> dodaneLinie = new ArrayList<>();

    public Controller() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        krokiSuwak.valueProperty().addListener((obs, staraWartosc, nowaWartosc) ->
                krokiSuwak.setValue(nowaWartosc.intValue()));

        stopnieSuwak.valueProperty().addListener((obs, staraWartosc, nowaWartosc) ->
                stopnieSuwak.setValue(nowaWartosc.intValue()));

        Image im = new Image(Main.class.getResourceAsStream("turtle.png"));
        zolw.setFill(new ImagePattern(im));

        stopnieSuwak.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number staraWartosc, Number nowaWartosc) {
                int obrocZolwiaOStopni = nowaWartosc.intValue() - staraWartosc.intValue();
                obrocZolwia(obrocZolwiaOStopni);
            }
        });
    }

    @FXML
    public void idzPrzyciskAkcja(ActionEvent actionEvent) {
        try {
            Integer kroki = wezAktualneKroki();
            Integer stopnie = wezAktualneStopnie();

            operacjaLabelka.setText("Dobra idę. Zrobię " + kroki + " kroków. Obrót " + stopnie);

            int startX = (int) Math.round(zolw.getCenterX());
            int startY = (int) Math.round(zolw.getCenterY());

            int koniecX = (int) Math.round(startX + kroki * Math.cos(stopnieNaRadiany(stopnie)));
            int koniecY = (int) Math.round(startY + kroki * Math.sin(stopnieNaRadiany(stopnie)));

            System.out.println("koniecX = " + koniecX);
            System.out.println("koniecY = " + koniecY);

            boolean czyNadalKoniecXJestWPolu = koniecX > 0 && koniecX <= wezWymiarPolaX();
            boolean czyNadalKoniecYJestWPolu = koniecY > 0 && koniecY <= wezWymiarPolaY();

            if (czyNadalKoniecXJestWPolu && czyNadalKoniecYJestWPolu) {
                przesunZolwiaNa(koniecX, koniecY);

                if (rysuj.isSelected()) {
                    dodajLinie(startX, startY, koniecX, koniecY);
                }
            } else {
                operacjaLabelka.setText("Nie mogę tam iść. To za daleko!");
            }
        } catch (Exception e) {
            operacjaLabelka.setText("Problem z " + e.getMessage());
        }
    }

    double stopnieNaRadiany(int a) {
        return (a * RADIAN);
    }

    @FXML
    public void wyczyscPrzyciskAkcja(ActionEvent actionEvent) {
        operacjaLabelka.setText("Czyszczę i wracam na początek");

        usunWszystkieLinie();
        przesunZolwiaNaPoczatek();
    }

    private void przesunZolwiaNaPoczatek() {
        przesunZolwiaNa(0, 0);
        stopnieSuwak.setValue(45);
        krokiSuwak.setValue(10);
    }

    private void usunWszystkieLinie() {
        plac.getChildren().removeAll(dodaneLinie);
        dodaneLinie.clear();
    }

    private void przesunZolwiaNa(int x, int y) {
        zolw.setCenterX(x);
        zolw.setCenterY(y);
    }

    private void dodajLinie(int startX, int startY, int koniecX, int koniecY) {
        Line linia = new Line(startX, startY, koniecX, koniecY);
        linia.setStyle("-fx-stroke: #298640;");

        plac.getChildren().addAll(linia);
        dodaneLinie.add(linia);
    }

    public int wezWymiarPolaX() {
        return (int) Math.round(plac.getWidth());
    }

    public int wezWymiarPolaY() {
        return (int) Math.round(plac.getHeight());
    }

    public int wezAktualneStopnie() {
        return (int) Math.round(Double.parseDouble(stopniePole.getText()));
    }

    public int wezAktualneKroki() {
        return (int) Math.round(Double.parseDouble(krokiPole.getText())) + KROK_W_PIKSELACH;
    }

    public void obrocZolwia(int dodatkowyKont) {
        zolw.setRotate(zolw.getRotate() + dodatkowyKont);
    }
}
